<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'firsttask' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'HlER*U_?PD@-&<n!.,ut;P[b7g1VqSYf94nZP7d*f]?tXn?IexLSw$PT5w*To/g#' );
define( 'SECURE_AUTH_KEY',  'p;-9+?F?6:0x~0/r(?u;ui%x$(/hmM7N0oJn}/S&aH=R_+`{{x-]>S2fCRN&W{n7' );
define( 'LOGGED_IN_KEY',    'n#j5}LCh_bDbCN^>vw8J<|ojvYQaOo[6-E1t;bz:ZcqHb~Tp>}M@@AI(O?zU%m5<' );
define( 'NONCE_KEY',        'F`L0N5Z&!q*WXFM9)C~t:M;e8<-[`7tmz,&=qw+&LX}tvY~B6.NR,OD%>7]-t>pB' );
define( 'AUTH_SALT',        'rwQW990WSu/SzY ]>i.bKi?LSy[Zg:MJ~.A6*W=)Q8nE)boP%X)L/Tlv:fQe>c(d' );
define( 'SECURE_AUTH_SALT', '3ifA3<C[DC_^RVAV@N[;&~gP=D%$;;/f.&=o&5Kk(k+89VyL-#RXP5Uc(iP99on(' );
define( 'LOGGED_IN_SALT',   '0:)(xXKzGmM!k*J}=lHk%nz+Ankx06mND5|`b#N:jVJ8vw)P{AGM^39jE0?g:pf[' );
define( 'NONCE_SALT',       '`c4xz?/pix)WnSjg1&>SrEZP27WX&l[P;1[&S$y`X-g(mEsGvG-x-mON%Yh!V-PR' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
